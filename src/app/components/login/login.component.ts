import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required, Validators.minLength(3) ]),
    password: new FormControl('', [ Validators.required, Validators.minLength(6) ])
  });

  isLoading: boolean = false;
  loginError: string;

  constructor(private session: SessionService, private AuthService: AuthService, private router: Router) { 

    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard');
    }

  }

  ngOnInit(): void {
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  async onLoginClicked() {
    this.loginError = '';
    try {
      this.isLoading = true;
      const result: any = await this.AuthService.login(this.loginForm.value);
      if ( result.status < 400 ) {
        console.log('Successful login')
        this.session.save( { token: result.data.token, username: result.data.username } );
        this.router.navigateByUrl('/dashboard');        
      } else {
        alert('Login attempt failed');
      }
    } catch (e) {
      this.loginError = e.error.error;
    } finally {
      this.isLoading = false;
    }
  }

  onRegisterClicked() {
    this.router.navigateByUrl('/register');
  }

}
