import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  // registerForm: FormGroup = new FormGroup({
  //   user: <any> {
  //     username: new FormControl('', [ Validators.required, Validators.maxLength(3) ]),
  //     password: new FormControl('', [ Validators.required, Validators.maxLength(6) ])
  //   }, 
  //   repeatedPassword: new FormControl('', [ Validators.required, Validators.maxLength(6) ])
  // });

  registerForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required, Validators.minLength(3) ]),
    password: new FormControl('', [ Validators.required, Validators.minLength(6) ]), 
    repeatedPassword: new FormControl('', [ Validators.required, Validators.maxLength(6) ])
  });
  
  // user = {
  //   username: '',
  //   password: ''
  // };

  isLoading: boolean = false;
  registerError: string;

  // public repeatedPassword: string = '';

  // Injecting services to the component
  constructor(private AuthService: AuthService, private session: SessionService, private router: Router) { 
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard');
    }
  }

  ngOnInit(): void {
  }

  get username() {
    return this.registerForm.get('username');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get repeatedPassword() {
    return this.registerForm.get('repeatedPassword');
  }




  async onSaveClicked() {
    this.registerError = '';
    try {
      this.isLoading = true;
      const result: any = await this.AuthService.register(this.registerForm.value );
      if (result.status < 400) {
        this.session.save({
          token: result.data.token,
          username: result.data.user.username
        });
        console.log('Resultat från API');
        console.log(result);
        this.router.navigateByUrl('/dashboard');
      }
    } catch (e) {
      console.error(e.error);
      this.registerError = e.error.error;
    } finally { // Finally runs regardless of success or not
      this.isLoading = false;
    }
  }

  onCancelClicked() {
    this.router.navigateByUrl('/login');
  }

}
